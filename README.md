Proteus example data
====================

Datasets for different setups to test the Proteus software.

Available datasets
------------------

*   *unigetel_dummy*

    Geneva telescope with six IBL FE-I4 modules as the telescope layers
    and a seventh IBL FE-I4 module as a dummy device-under-test at the
    center. The same geometry was simulated for multiple beam conditions
    using [Allpix²](https://cern.ch/allpix-squared). Data is provided in
    the RCE Root format.
 
*   *MALTAscope*
 
    Five MALTA sensors with 512x512 pixels.

